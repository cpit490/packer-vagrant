#!/bin/bash
apt update
apt upgrade -y
apt install -y nano
# Install PHP 8.3, we will add Ondrej Sury's PPA into the system
apt install ca-certificates apt-transport-https software-properties-common
add-apt-repository ppa:ondrej/php
apt update
apt install -y php8.3
apt install -y php8.3-{cli,bz2,mysql,intl,xml,zip,gd,mbstring,curl,xmlrpc,soap,fpm}
apt install -y apache2
apt install -y mariadb-server